val scala3Version = "3.1.3"


lazy val root = project
  .in(file("."))
  .enablePlugins(NativeImagePlugin)
  .settings(
    name := "bubblebabble",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    nativeImageOptions  += "--no-fallback",
    libraryDependencies += "co.fs2" %% "fs2-core" % "3.2.11",
    libraryDependencies += "org.scalameta" %% "munit" % "0.7.29" % Test,
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.16.0" % Test,
    testFrameworks += new TestFramework("munit.Framework"),
  )
