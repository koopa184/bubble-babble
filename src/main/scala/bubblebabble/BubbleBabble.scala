package bubblebabble

import fs2.{Chunk, Pipe, Pull, Pure, Stream}
import fs2.text.utf8

object BubbleBabble {
  case class EncoderState(previousChecksum: Byte, carryoverByte: Option[Byte])

  sealed trait BubbleBabbleTuple
  case class FullTuple(a: Byte, b: Byte, c: Byte, d: Byte, e: Byte) extends BubbleBabbleTuple
  case class PartialTuple(a: Byte, b: Byte, c: Byte) extends BubbleBabbleTuple

  val StartingChecksum = 1.toByte

  val vowelTable:  Map[Byte, String] =
    Vector("a", "e", "i", "o", "u", "y")
      .zipWithIndex
      .map { case (s, i) => i.toByte -> s}
      .toMap

  val consonantTable:  Map[Byte, String] = Vector("b", "c", "d", "f", "g", "h", "k", "l", "m", "n", "p", "r", "s", "t", "v", "z", "x")
    .zipWithIndex
    .map { case (s, i) => i.toByte -> s}
    .toMap

  val consonantToByteTable = consonantTable.map { case (k, v) => v -> k }.toMap
  val vowelToByteTable = vowelTable.map { case (k, v) => v -> k }.toMap
  private def vowelByte(c: Char) = vowelToByteTable(c.toString)
  private def consonantByte(c: Char) = consonantToByteTable(c.toString)

  /*
   * Quick sketch of the algorithm:
   *
   * At any point in time while we're calculating the bubblebabble digest, we need this data:
   *    - The previous value of the checksum
   *    - The current two bytes in the data (in case of a non-partial tuple) or
   *    the final byte of data (in case of the partial tuple of odd-length data)
   *    - Because of the nature of Streams/chunking, an Option[Byte] to represent any
   *    carry-over from a previous processing step
   * 
   * The fs2.Stream type is internally "chunked" for performance, and to be able to
   * take advantage of that and not make things slightly less performant, we'll make
   * our algorithm "chunk-aware" and work on a chunked version of the Stream[F, Byte]
   * that we get.
   * 
   * Using stream.pull.uncons, we can statefully request the next chunk and do
   * all of our processing on groups of 2 bytes within those chunks. If the
   * chunk has an odd number of bytes (or if we have a carry-over byte from some
   * previous group of chunks, and this chunk's size is even), we can take the
   * carry-over byte at the end, and store it within our state.
   *
   * At the end, when there are no more whole Chunks left to consume from the Stream, we
   * will either have one carry-over byte (if the overall number of bytes in the
   * Stream was odd) or none; in the case where we have one, we can construct the
   * "odd case" partial tuple, and if we have none, we create the "even case" partial
   * tuple.
   */
  def encodeAsStream[F[_]]: Pipe[F, Byte, String] = (in: Stream[F, Byte]) => {
    def go(s: Stream[F, Byte], encoderState: EncoderState): Pull[F, BubbleBabbleTuple, Unit] = {
      s.pull.uncons.flatMap {
        // If we've gotten a chunk, we do the processing we need to it
        // (emitting all of the full tuples within that chunk)
        case Some(hd, tl) => {
          val (newEncoderState, pull) = processChunk(hd, encoderState)
          pull >> go(tl, newEncoderState)
        }
        // If we haven't, we emit the partial tuple (making sure to also use any
        // carry-over byte, if the total number of bytes is odd).
        case None => {
          Pull.output(Chunk(getPartialTuple(encoderState)))
        }
      }
    }

    def processChunk[F[_]](chunk: Chunk[Byte], encoderState: EncoderState): (EncoderState, Pull[F, BubbleBabbleTuple, Unit]) = {
      val byteChunk = Chunk.fromOption(encoderState.carryoverByte) ++ chunk
      val groupedBytes = (0 until byteChunk.size / 2).view.map(i => (byteChunk(i * 2), byteChunk(i * 2 + 1)))
      val groupedChunk = Chunk.iterator(groupedBytes.to(Iterator))
      val (finalChecksum, tuplesToEmit) = groupedChunk.mapAccumulate(encoderState.previousChecksum) { (previousChecksum, currentElement) =>
        val (byte1, byte2) = currentElement
        val (tuple, nextChecksum) = makeTupleAndChecksum(byte1, byte2, previousChecksum)
        (nextChecksum, tuple)
      }
      val carryoverByte = Option.when(byteChunk.size % 2 == 1)(byteChunk(byteChunk.size - 1))
      (EncoderState(finalChecksum, carryoverByte), Pull.output(tuplesToEmit))
    }

    def makeTupleAndChecksum(b1: Byte, b2: Byte, previousChecksum: Byte): (FullTuple, Byte) = {
      val chunk = FullTuple(
        ((((b1 >> 6) & 3) + previousChecksum) % 6).toByte,
        ((b1 >> 2) & 15).toByte,
        (((b1 & 3) + previousChecksum / 6) % 6).toByte,
        ((b2 >> 4) & 15).toByte,
        (b2 & 15).toByte,
      )
      val nextChecksum = getNextChecksum(b1, b2, previousChecksum)
      (chunk, nextChecksum)
    }

    def encodeTupleAsString(tuple: BubbleBabbleTuple): String = {
      val vt = vowelTable
      val ct = consonantTable
      tuple match {
        case FullTuple(a, b, c, d, e) => Vector(vt(a), ct(b), vt(c), ct(d), "-", ct(e)).mkString
        case PartialTuple(a, b, c) => Vector(vt(a), ct(b), vt(c)).mkString
      }
    }

    def getPartialTuple(encoderState: EncoderState): PartialTuple = {
      encoderState.carryoverByte.fold {
        PartialTuple(
          ((encoderState.previousChecksum & 255) % 6).toByte,
          // 16, AFAICT, marks for the decoder whether or not we had an even or odd number of bytes originally,
          // since in the odd case we "& 15" which cuts things off at 15.
          16.toByte,
          ((encoderState.previousChecksum & 255) / 6).toByte,
        )
      } { carryoverByte =>
        val cb = carryoverByte
        PartialTuple(
          ((((cb >> 6) & 3) + encoderState.previousChecksum) % 6).toByte,
          ((cb >> 2) & 15).toByte,
          (((cb & 3) + encoderState.previousChecksum / 6) % 6).toByte,
        )
      }
    }

    val mainStream = go(in, EncoderState(previousChecksum = StartingChecksum, carryoverByte = None))
      .stream
      .map(encodeTupleAsString)
    Stream("x") ++ mainStream ++ Stream("x")
  }

  def getNextChecksum(b1: Byte, b2: Byte, previousChecksum: Byte): Byte = {
    // We need to coerce our bytes to "unsigned" bytes stored in integers here,
    // since the original algorithm is specified in terms of unsigned numbers.
    ((previousChecksum * 5 + ((b1 & 255) * 7 + (b2 & 255))) % 36).toByte
  }

  def encode[F[_]]: Pipe[F, Byte, String] = (in: Stream[F, Byte]) => {
    // Technically a little weird to fold using something mutable here,
    // but it's better performance for making the string instead of O(N^2)
    // concatenation complexity
    encodeAsStream(in).fold(new StringBuilder){
    case (stringBuilder, string) => stringBuilder.addAll(string)
    }.map(_.toString)
  }

  def encodeString(s: String): String = {
    encode(utf8.encode(Stream(s)))
      .compile
      .toList
      .headOption
      .getOrElse("")
  }

  def modularSubtract(x: Int, y: Int, modulus: Int): Int = {
    val difference = (x % modulus) - (y % modulus)
    if (difference < 0) {
      modulus + difference
    } else {
      difference
    }
  }


  def decodeStreamToTuples[F[_]]: Pipe[F, String, BubbleBabbleTuple] = (in: Stream[F, String]) => {
    def go(stream: Stream[F, String], carryoverString: Option[String]): Pull[F, BubbleBabbleTuple, Unit] = {
      stream.pull.uncons.flatMap {
        case Some(head, tail) => {
          val (tupleChunk, newCarryover) = decodeChunkToTuples(head)
          Pull.output(tupleChunk) >> go(tail, newCarryover)
        }
        case None =>
          Pull.outputOption1(carryoverString.map(s => decodePartialTuple(s.filter(_.isLetter))))
      }
    }

    def decodeChunkToTuples(chunk: Chunk[String]): (Chunk[BubbleBabbleTuple], Option[String]) = {
      val (finalCarryover, chunks) = chunk.mapAccumulate(Option.empty[String]) {  (carryoverString, currentElement) =>
        val stringForDecoding = carryoverString.map(_ ++ currentElement).getOrElse(currentElement)
        val (tupleChunk, newCarryover) = decodeStringToTuples(stringForDecoding)
        (newCarryover, tupleChunk)
      }
      (chunks.flatMap(identity), finalCarryover)
    }

    def decodeStringToTuples(s: String): (Chunk[BubbleBabbleTuple], Option[String]) = {
      sealed trait TupleOrCarryover
      case class EmittedTuple(b: BubbleBabbleTuple) extends TupleOrCarryover
      case class Carryover(s: String) extends TupleOrCarryover
      val tuplesAndCarryover = s.filter(_.isLetter).grouped(5).map { chars =>
        if (chars.size == 5) {
          EmittedTuple(FullTuple(
            vowelByte(chars(0)),
            consonantByte(chars(1)),
            vowelByte(chars(2)),
            consonantByte(chars(3)),
            consonantByte(chars(4)),
          ))
        } else {
          Carryover(chars)
        }
      }
      val (tuples, carryover) = tuplesAndCarryover.partition {
        case e: EmittedTuple => true
        case _ => false
      }
      (Chunk.iterator(tuples.collect { case EmittedTuple(t) => t }), carryover.nextOption().collect { case Carryover(s) => s})
    }

    def decodePartialTuple(s: String): PartialTuple = {
      PartialTuple(
        vowelByte(s(0)),
        consonantByte(s(1)),
        vowelByte(s(2)),
      )
    }
    val streamToTransform = in.map(_.filter(_.isLetter))

    // Bubble Babble encoding puts an 'x' on the front and back of the string; we remove that here.
    val streamWithoutSurroundingX = {
      val withHeadTransformed = streamToTransform.head.map(_.dropWhile(_ == 'x')) ++ streamToTransform.drop(1)
      withHeadTransformed.dropLast ++ withHeadTransformed.lastOr("").map(_.reverse.dropWhile(_ == 'x').reverse)
    }

    go(streamWithoutSurroundingX, carryoverString = None).stream
  }

  def decode[F[_]]: Pipe[F, String, Byte] = (in: Stream[F, String]) => {
    decodeStreamToTuples(in)
      .mapAccumulate(StartingChecksum) { (previousChecksum, elem) =>
        val (chunk, newChecksum) = tupleToBytes(elem, previousChecksum)
        (newChecksum, chunk)
      }.map(_._2).unchunks
  }

  def decodeString(s: String): Stream[Pure, Byte] = {
    decode(Stream(s))
  }

  def tupleToBytes(tuple: BubbleBabbleTuple, previousChecksum: Byte): (Chunk[Byte], Byte) = {
    tuple match {
      case FullTuple(a, b, c, d, e) => {
        val byte1Top2Bits = (modularSubtract(a, previousChecksum, 6)) << 6
        val byte1Middle4Bits = b << 2
        val byte1Bottom2Bits = modularSubtract(c, previousChecksum / 6, 6)
        val byte1 = (byte1Top2Bits | byte1Middle4Bits | byte1Bottom2Bits).toByte
        val byte2TopHalf = d << 4
        val byte2BottomHalf = e
        val byte2 = (byte2TopHalf | byte2BottomHalf).toByte
        (Chunk(byte1, byte2), getNextChecksum(byte1, byte2, previousChecksum))
      }
      case PartialTuple(a, b, c) => {
        // `b` is only equal to 16 when we've had an even number of bytes, and the
        // checksum info is the only thing we store in the last partial tuple
        if (b == 16) {
          // We emit 0 for our checksum because we should be on the last tuple,
          // and it should no longer matter.
          (Chunk.empty[Byte], 0)
        } else {
          val topBits = modularSubtract(a, previousChecksum, 6) << 6
          val middleBits = b << 2
          val bottomBits = modularSubtract(c, previousChecksum / 6, 6)
          (Chunk((topBits | middleBits | bottomBits).toByte), 0)
        }
      }
    }
  }
}
