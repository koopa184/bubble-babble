import bubblebabble.BubbleBabble
import fs2.Stream
import fs2.text.utf8
import org.scalacheck.Prop.forAll
import org.scalacheck.Test
import org.scalacheck.util.Pretty
import scala.util.Random

class BubbleBabbleEncoding extends munit.FunSuite {
  test("handles the empty string correctly") {
    assertEquals(BubbleBabble.encodeString(""), "xexax")
  }
  test("produces the correct output for a certain string") {
    assertEquals(
      BubbleBabble.encodeString("To be, or not to be? That is the question."),
      "xihak-zimok-donyd-symik-zesad-boruk-zutid-bitok-zumek-denof-zimah-gepyk-cutad-bipal-fomyl-gipak-hamel-citik-hisal-gopik-zyryd-vixux",
    )
  }
  test("produces the correct output even if chunk sizes are of odd length") {
    val oddChunkByteStream = utf8.encode(Stream("Hello, world!")).chunkN(3).unchunks
    assertEquals(
      BubbleBabble.encode(oddChunkByteStream).fold("")(_ ++ _).compile.toList.head,
      "xidak-hyryk-sored-somil-lyral-daruk-gymyx",
    )
  }
  test("produces the correct output even if chunks produce multiple tuples") {
    val oddChunkByteStream = utf8.encode(Stream("Murder, she wrote" * 3)).chunkN(7).unchunks
    assertEquals(
      BubbleBabble.encode(oddChunkByteStream).fold("")(_ ++ _).compile.toList.head,
      "xifel-hisuk-gunyl-dirod-bosuk-minod-betyl-daril-genyg-tytel-danik-hysyd-sumil-fapok-himel-lasyk-zytik-huful-hasik-ganul-darad-bysyk-manod-botil-durel-gonux",
    )
  }
  test("decodes to the empty string correctly") {
    assertEquals(
      utf8.decode(BubbleBabble.decodeString("xexax")).toList.mkString,
      "",
    )
  }
  test("decodes a certain string correctly") {
    assertEquals(
      utf8.decode(BubbleBabble.decodeString("xihak-zimok-donyd-symik-zesad-boruk-zutid-bitok-zumek-denof-zimah-gepyk-cutad-bipal-fomyl-gipak-hamel-citik-hisal-gopik-zyryd-vixux")).toList.mkString,
      "To be, or not to be? That is the question.",
    )
  }

  test("encodes for a particular weird Unicode character") {
    assertEquals(
      BubbleBabble.encodeString("ã"),
      "xubop-foxox",
    )
  }

  test("encoding and then decoding a string should produce the same string as output") {
    val propToCheck = forAll { (s: String) =>
      utf8.decode(BubbleBabble.decodeString(BubbleBabble.encodeString(s))).compile.toList.mkString == s
    }
    val result = Test.check(Test.Parameters.default, propToCheck)
    assert(result.passed == true, Pretty.pretty(result))
  }

  test("works for a longer string") {
    val longString = Random.alphanumeric.take(2000).mkString
    assertEquals(
      utf8.decode(BubbleBabble.decodeString(BubbleBabble.encodeString(longString))).compile.toList.mkString,
      longString,
    )
  }
}