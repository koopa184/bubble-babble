## Bubble Babble

Bubble Babble is an encoding that turns binary data into groups of vowels and consonants,
which makes it both readable and pronounceable. You can find out more about the encoding
by looking at [its specification](https://web.mit.edu/kenta/www/one/bubblebabble/spec/jrtrjwzi/draft-huima-01.txt).

This is a re-implementation of the Bubble Babble algorithm that takes [fs2](https://fs2.io/) streams as input.
The algorithm is chunk-aware, which makes it at least somewhat more efficient. Also, because the algorithm is
expressed in terms of the `Stream[F, Byte]` type, it means that it's easily reused in a variety of contexts where
you might have a stream of bytes; it could be used to encode data coming from an HTTP request in Http4s,
from a file, a string, a random byte generator, etc., all while consuming only a fixed amount of memory instead
of the memory scaling with the input size.